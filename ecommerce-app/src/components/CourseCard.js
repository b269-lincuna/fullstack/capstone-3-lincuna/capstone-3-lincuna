/*
//[S50 Activity]

import { Row, Col, Card , Button } from 'react-bootstrap';

export default function CourseCard() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
		                <Card.Title>
		                    <h2>React.js</h2>
		                </Card.Title>
	                    <Card.Text>
	                        <p>Description:</p>
	                        <p>This is a sample course offering.</p>
	                        <p>Price:</p>
	                        <p>PHP 40,000</p>
	                    </Card.Text>
	                    <Button variant="primary">Enroll</Button>
	                </Card.Body>
	                
	            </Card>
	        </Col>
	        
	    </Row>
	)
}
//[S50 Activity END]
*/


import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
/*export default function CourseCard({course}) {

	// Destructure
	const {name, description, price} = course;

	const [count, setCount] = useState(0);

	function enroll (){
		setCount(count+1);
	};


	return (
	    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PHP {price}</Card.Text>
                        <Card.Subtitle>Count: {count}</Card.Subtitle>
                        <Button variant="primary" onClick={enroll}>Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>      
	    </Row>        
    )
}*/

// [S50 ACTIVITY END]

// [S51 ACTIVITY START]
export default function CourseCard({course}) {

	// Destructure
	const {name, description, price, _id } = course;

	// const [enrollCount, setEnrollCount] = useState(0);
	// const [slotCount, setSeatCount] = useState(30);

	// function enroll (){
	// 	// if(slotCount!==0){
	// 	// 	setEnrollCount(enrollCount+1);
	// 	// 	setSeatCount(slotCount-1);
	// 	// } else {
	// 	// 	alert("No more seats available.");
	// 	// }

	// 	setEnrollCount(enrollCount+1);
	// 	setSeatCount(slotCount-1);


	// };

// useEffect(() =>{
// 	if(slotCount <= 0 ){
// 		alert("No more seats available!");
// 	}
// }, [slotCount]);

	return (
	    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>PHP {price}</Card.Text>
                        {/*<Card.Subtitle>Enrollees:</Card.Subtitle>
                        <Card.Text>{enrollCount}</Card.Text>*/}
                        {/*<Button variant="primary" onClick={enroll} disabled={slotCount <= 0}>Enroll
                        </Button>*/}
                        <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details
                        </Button>
                    </Card.Body>
                </Card>
            </Col>
	    </Row>
    )
}


//<Button variant="primary" onClick={enroll} disabled={slotCount === 0}>
//	{slotCount === 0 ? "Sold Out" : "Enroll"}
//</Button>
// [S51 ACTIVITY END]

