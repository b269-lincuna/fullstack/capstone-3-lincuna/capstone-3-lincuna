import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3 ">
	        <Col xs={12} md={4} >
	            <Card className="cardHighlight p-3 h-100 ">
	            	<Card.Img  style={{ height: "300px" }} variant="top" src="https://m.media-amazon.com/images/I/7128GOUxSCL._AC_SL1500_.jpg" />
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Play from Home</h2>
	                    </Card.Title>
	                    <Card.Text>
	                         Pro Gaming PC Computer Desktop SlateMR 215a (AMD Ryzen 5 5600G 3.9 GHz,AMD Radeon RX 6600XT 8GB, 16GB DDR4, 480 GB SSD, WiFi Ready, Windows 11 Home)
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4} >
	            <Card className="cardHighlight p-3 h-100 ">
	            	{/*<Card.Img variant="top" src="../images/highlight/1.png" />*/}
	            	<Card.Img  style={{ height: "300px" }} variant="top" src="https://www.zotac.com/us/system/files/styles/w1024/private/product_gallery/graphics_cards/zt-d40710b-10p-image01.jpg?itok=XPLlORmF" />
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Experience High Graphics</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        ZOTAC GAMING GeForce RTX 4070 Ti AMP Extreme AIRO | ZOTAC
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3 h-100">
	            	<Card.Img  style={{ height: "300px" }} variant="top" src="https://media.istockphoto.com/id/1217530320/vector/flat-design-young-gamer-playing-online-video-game-wearing-headphone.jpg?s=612x612&w=0&k=20&c=X5Q0dzichccfNZarkpIWdxw9ActVGWMM9jER8F01lk4=" />
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Be Part of Our Community</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        ReedPOP Expands Its Gaming Community Presence with Gamer Network Acquisition | TSNN Trade Show News
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}


//<Card.Img variant="top" src="../images/highlight/1.png" />
