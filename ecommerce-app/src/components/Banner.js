
import { Button, Row, Col } from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';

export default function Banner({notFound}) {
    
    const navigate = useNavigate();

    

    return (
        (notFound) ?
            <>
                <Row>
                    <Col className="p-5">
                        <h1>Error 404 - page not found.</h1>
                        <p>The page you are looking for cannot be found.</p>
                        <Button variant="primary" onClick={() => navigate('/')} >Back to Home</Button>
                    </Col>
                </Row>
            </>
        :
            <>   
                <Row>
                    <Col className="p-5">
                        <h1>Welcome to Techno Shop</h1>
                        <p>Opportunities for everyone, everywhere.</p>
                        <Button variant="primary" onClick={() => navigate("/products")}>Order Now!</Button>
                    </Col>
                </Row>
            </>
    )
}

